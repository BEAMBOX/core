class CreateListings < ActiveRecord::Migration
  def change
    create_table :listings do |t|
      t.string :symbol
      t.string :name
      t.string :website
      t.string :sector
      t.string :subsector
      t.string :country

      t.timestamps
    end
  end
end
