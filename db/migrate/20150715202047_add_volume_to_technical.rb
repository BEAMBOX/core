class AddVolumeToTechnical < ActiveRecord::Migration
  def change
    add_column :technicals, :volume, :integer
    add_column :technicals, :date, :string
  end
end
