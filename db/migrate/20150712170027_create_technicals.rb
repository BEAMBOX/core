class CreateTechnicals < ActiveRecord::Migration
  def change
    create_table :technicals do |t|
      t.integer :listing_id
      t.decimal :open
      t.decimal :close
      t.string :symbol

      t.timestamps
    end
  end
end
