class ModuleOneController < ApplicationController

  def Initiation
    Listing.all.each do |listing|
      if listing.symbol.include?(".L") == false
        var = listing.symbol + ".L"
        listing.update(symbol: var)
      elsif listing.symbol.include?(".L.L") == true
        var = listing.symbol.slice!(".L.L")
        vared = listing.symbol + ".L"
        listing.update(symbol: vared)
      end
    end
  end

  def stockRake

  	@array = Array.new

  	Listing.all.each do |listing| #construct array to only make one get request
  		@array.push listing.symbol
  	end

  	@data = YahooFinance.quotes(@array, [:symbol, :close, :open, :volume, :trade_date]) #get request

  	@data.each_with_index do |data, index|
  		Technical.create(
  			listing_id: index + 1, #+1 to make up for index starting at 0
  			symbol: data.symbol,
  			open: data.open,
  			close: data.close,
        volume: data.volume,
        date: data.trade_date
  		)
  	end

  end

  def historicalRake
    Listing.where(id: 590..900).each do |listing|
      data = YahooFinance.historical_quotes(listing.symbol, { start_date: Time::now - 2.years, end_date: Time::now })
      data.each do |data|
        Technical.create(
          listing_id: listing.id,
          symbol: data.symbol,
          open: data.open,
          close: data.close,
          volume: data.volume,
          date: data.trade_date
        )
      end
    end
  end

  def printTest
  end

  def backTest

    @array = Array.new

    Listing.all.each do |listing|
      Technical.all.where(symbol: listing.symbol).each do |technical|

        lightOne = false
        lightTwo = false
        lightThree = false

        #LightOne start

        date = technical.date.to_date
        ninety = date - 90.days
        findDate = ninety.srftm("%Y-%m-%d")

        ninetyTechnical = Technical.find_by(symbol: listing.symbol, date: findDate)

        compare = technical.open

        if ninetyTechnical.open / now > 2
          lightOne = true
        end

        #LightOne end
        #LightTwo start

        twoWeek = Array.new






      end
    end

  end

end
